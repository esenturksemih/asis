﻿using AsisMarsRover.API.Models;
using AsisMarsRover.Business.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AsisMarsRover.API.Controllers
{
    public class MarsRoverController : ApiController
    {

        IHttpActionResult Index()
        {
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult GetResult(CommonRequestModel model)
        {

            TaskFactory factory = new TaskFactory();

            var area = factory.CreateArea(model.AreaWidth, model.AreaHeight);
            var rover = factory.CreateRover(area, model.Statement);

            rover.Process(model.Commands);


            CommonResponseModel responsemodel = new CommonResponseModel();
            responsemodel.ResultMessage = rover.ToString();
            return Ok(responsemodel);
        }

    }
}
