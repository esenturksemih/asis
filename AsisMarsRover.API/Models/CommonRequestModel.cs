﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AsisMarsRover.API.Models
{
    public class CommonRequestModel
    {
        //Area oluşturmak için ölçüler
        public int AreaWidth { get; set; }
        public int AreaHeight { get; set; }

        //Rover konumlandırma pozisyonu için ilk koordinatlar '1 3 S' gibi.
        public string Statement { get; set; }


        
        //public int X { get; set; }

        //public int Y { get; set; }

        //// E,W,S,N gibi
        //public string Code { get; set; }


        //LLMM gibi yönlendirme komutları
        public string Commands { get; set; }

    }
}