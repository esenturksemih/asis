﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AsisMarsRover.API.Models
{
    public class CommonResponseModel
    {
        public string ResultMessage { get; set; }
    }
}