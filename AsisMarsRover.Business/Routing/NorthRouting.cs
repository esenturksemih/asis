﻿using AsisMarsRover.Business.DomainModels;
using AsisMarsRover.Business.Routing.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsisMarsRover.Business.Routing
{
    public class NorthRouting: IRouting
    {
        public string Code => "N";

        public Location Move(Location location)
        {
            return new Location(location.X, location.Y + 1);
        }

        public IRouting TurnRight()
        {
            return new EastRouting();
        }

        public IRouting TurnLeft()
        {
            return new WestRouting();
        }
    }
}
