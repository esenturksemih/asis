﻿using AsisMarsRover.Business.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsisMarsRover.Business.Routing.Interface
{
    public interface IRouting
    {
        string Code { get; }
        Location Move(Location location);
        IRouting TurnRight();
        IRouting TurnLeft();
    }
}
