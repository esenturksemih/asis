﻿using AsisMarsRover.Business.DomainModels;
using AsisMarsRover.Business.Routing.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsisMarsRover.Business.Routing
{
    public class SouthRouting: IRouting
    {

        public string Code => "S";

        public Location Move(Location location)
        {
            return new Location(location.X, location.Y - 1);
        }

        public IRouting TurnRight()
        {
            return new WestRouting();
        }

        public IRouting TurnLeft()
        {
            return new EastRouting();
        }
    }
}
